package com.ruoyi.framework.config;

import com.wf.captcha.ArithmeticCaptcha;
import com.wf.captcha.SpecCaptcha;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 验证码配置
 *
 * @author ruoyi
 */
@Configuration
public class CaptchaConfig {
    @Bean(name = "captchaProducer")
    public SpecCaptcha getCaptchaBean() {
        SpecCaptcha captcha = new SpecCaptcha();
        return captcha;
    }

    @Bean(name = "captchaProducerMath")
    public ArithmeticCaptcha getCaptchaBeanMath() {
        ArithmeticCaptcha captcha = new ArithmeticCaptcha();
        return captcha;
    }
}
